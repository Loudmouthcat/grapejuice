from grapejuice_common.models.launch_uri import LaunchUri
from grapejuice_common.roblox_product import RobloxReleaseChannel


def test_parse_uri_works(a_launch_uri):
    model = LaunchUri(a_launch_uri)

    v = model.as_string

    assert v == a_launch_uri


def test_can_set_channel(a_launch_uri):
    model = LaunchUri(a_launch_uri)
    channel = RobloxReleaseChannel.Integration

    model.channel = channel
    v = model.as_string

    assert v != a_launch_uri
    assert channel.value in v


def test_can_add_uri_property(a_launch_uri_no_channel):
    model = LaunchUri(a_launch_uri_no_channel)
    channel = RobloxReleaseChannel.Integration

    model.channel = channel
    v = model.as_string

    assert v != a_launch_uri_no_channel
    assert channel.value in v
    assert "+channel" in v.lower()
